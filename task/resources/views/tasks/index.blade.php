@extends('layouts.master')
@section('content')
<div class="row justify-content-md-center mt-5">
    <div class="col-md-6 ">
      {{-- @if ($errors->any())
      
      @foreach($errors->all() as $error)
      <div class="alert alert-danger">
        {{$error}}
      </div>

      @endforeach
          
      @endif --}}
      @if (session()->has('msg'))
      <div class="alert alert-danger">
        {{session()->get('msg')}}
      </div>
          
      @endif
<div class="card text-center">
        <div class="card-header   h3">
          ADD TASKS
        </div>
        <div class="card-body">
          
        <form action="{{route('task.create')}}" method="post">
              @csrf
          <div class="form-group">
                  <label class="text-center" for="task">Task</label>
                  <input type="text" name="title" id="task" placeholder="Task" class="form-control {{ $errors->has('title' )? 'is-invalid':''}} ">
              <div class="invalid-feedback text-danger">
               {{ $errors->has('title' )? $errors->first('title'):''}}

              </div>
                </div>
              <div class="form-group col-sm-3">
                    
                    <input type="+ Add" class="form-control btn btn-primary" >
                </div>
          </form>
          
        </div>
      
      </div>
    </div>
</div>


















<div class="row justify-content-md-center mt-5">
  <div class="col-md-6 ">
<div class="card text-center">
      <div class="card-header   h3">
        pending Task
      </div>
      <div class="card-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Tasks</th>
              <th style="width:2em">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($tasks as $item)
                
            
            <tr>
            <td>{{$item->title}}</td>
            
            <form action="{{route('task.delete', $item->id)}}" method="POST">
              @csrf
              @method('delete')
              <td><button type="submit" class="btn btn-danger btn-sm">Delete</button></td>
            </form>
            </tr>
            @endforeach
          </tbody>
        </table>
        
      </div>
    
    </div>
  </div>
</div>

    
@endsection